"use strict"

/*
Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
Метод event.preventDefault() у JavaScript використовується для того, щоб скасувати стандартну дію події, яка зазвичай відбувається відповідно до браузерних властивостей.
Коли ви викликаєте event.preventDefault() у обробнику подій, ви запобігаєте виконанню типової дії, яка пов'язана з цією подією. 
Наприклад, якщо це викликається у обробнику події для кнопки, яка розташована в межах форми, це може запобігти відправці форми.

Приклад:
document.getElementById('myForm').addEventListener('submit', function(event) {
    // Запобігаємо відправці форми
    event.preventDefault();

    // Додаткові дії, які ви хочете виконати при події
});
Це дозволяє виконувати власний код замість того, щоб дозволити браузерові виконувати стандартні дії.

2. В чому сенс прийому делегування подій?
Прийом делегування подій у JavaScript — це підхід, при якому обробка подій встановлюється на батьківському елементі замість на кожному окремому дочірньому елементі. Ось декілька сенсів використання цього прийому:

1) Ефективність пам'яті і продуктивності:
Замість того, щоб прикріплювати обробник подій до кожного елемента, можна прикріпити один обробник до батьківського елемента. Це особливо корисно, коли в документі є багато елементів. 
Обробка подій на вищому рівні ієрархії може зменшити кількість обробників, які слухають події.

2) Динамічний контент:
Якщо нові елементи додаються або видаляються динамічно (наприклад, за допомогою JavaScript), обробка подій на батьківському елементі залишається нещадною незалежно від того, які елементи з'являються чи зникають.

3) Зменшення коду:
Код стає більш компактним і легше читається, оскільки вам не потрібно повторювати той самий обробник для багатьох елементів.

4) Зменшення вкладеності обробників:
Можна уникнути глибокої вкладеності обробників, що полегшує розуміння коду та управління ним.
Зручна реакція на події від різних елементів:
Використання делегування дозволяє легко реагувати на події від різних типів елементів, навіть якщо вони додаються динамічно.

Приклад використання делегування подій у JavaScript може виглядати наступним чином:
<ul id="myList">
  <li>Item 1</li>
  <li>Item 2</li>
  <li>Item 3</li>
</ul>

<script>
  document.getElementById('myList').addEventListener('click', (event) => {
    if (event.target.tagName === 'LI') {
      console.log('Clicked on:', event.target.innerText);
    }
  });
</script>
В цьому прикладі один обробник подій встановлено для всього списку <ul>, і він визначає, на якому саме <li> було клікнуто.

3. Які ви знаєте основні події документу та вікна браузера?
Основні події документу та вікна браузера в JavaScript включають такі:

Події документу (Document Events):
1) DOMContentLoaded:
Виникає, коли HTML документ повністю завантажено та розібрано, без очікування завершення завантаження таблиці стилів, зображень та під-документів (фреймів).
document.addEventListener('DOMContentLoaded', function() {
    // Викликається, коли DOM повністю завантажено
});

2) load:
Виникає, коли весь контент сторінки (зображення, стилі, скрипти) був завантажений.
window.addEventListener('load', function() {
    // Викликається, коли весь контент сторінки завантажено
});

Події вікна браузера (Window Events):
1) resize:
Виникає, коли розміри вікна браузера змінюються.
window.addEventListener('resize', function() {
    // Викликається при зміні розмірів вікна
});

2) scroll:
Виникає при прокручуванні сторінки.
window.addEventListener('scroll', function() {
    // Викликається при прокручуванні сторінки
});

3) unload:
Виникає, коли користувач покидає сторінку (закриває вкладку чи виходить зі сторінки).
window.addEventListener('unload', function() {
    // Викликається при закритті вкладки або виході зі сторінки
});

4) beforeunload:
Виникає перед тим, як сторінка буде розгружена. Зазвичай використовується для попередження користувача про можливу втрату даних.
window.addEventListener('beforeunload', function(event) {
    // Викликається перед розгрузкою сторінки
    // Можна використовувати для виведення попередження
    // event.returnValue = ''; // Для старих браузерів
});

5) orientationchange:
Виникає, коли користувач повертає пристрій.
window.addEventListener('orientationchange', function() {
    // Викликається при зміні орієнтації пристрою
});
Це лише кілька прикладів подій для документу та вікна браузера. Зазвичай розробники використовують ці події для взаємодії з користувачем та здійснення різних операцій у веб-додатках.

/*
Практичне завдання:
Реалізувати перемикання вкладок (таби) на чистому Javascript.
Технічні вимоги:
- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
 При цьому решта тексту повинна бути прихована. 
У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. 
При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
Умови:
- При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

document.querySelector('.tabs').addEventListener('click', (event) => {
    document.querySelectorAll('.tabs > li').forEach((el) => {
        el.classList.remove('active');
    });

    const currentEl = event.target;
    currentEl.classList.add('active');

    const elActive = document.querySelector('.tabs-content .active');
    if(elActive) {
        elActive.classList.remove('active');
    }

    const currText = document.querySelector(`.tabs-content > li[data-tab=${currentEl.id}]`);
    currText.classList.add('active'); 
});